# com.hcnx.hello

[![CI Status](http://img.shields.io/travis/Guillaume MARTINEZ/com.hcnx.hello.svg?style=flat)](https://travis-ci.org/Guillaume MARTINEZ/com.hcnx.hello)
[![Version](https://img.shields.io/cocoapods/v/com.hcnx.hello.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.hello)
[![License](https://img.shields.io/cocoapods/l/com.hcnx.hello.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.hello)
[![Platform](https://img.shields.io/cocoapods/p/com.hcnx.hello.svg?style=flat)](http://cocoapods.org/pods/com.hcnx.hello)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

com.hcnx.hello is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "com.hcnx.hello"
```

## Author

Guillaume MARTINEZ, g.martinez@highconnexion.com

## License

com.hcnx.hello is available under the MIT license. See the LICENSE file for more info.
